<?php
/*
Plugin Name: Skyrocket Radio Youtube Preroll Video
Plugin URI: http://www.skyrocketradio.com
Description: Integrates Youtube preroll video functionality into video embeds.  Usage and settings can be found under <strong>"Settings > Media"</strong>. 
Version: 1.0
Author: Skyrocket Radio
Author URI: https://www.skyrocketradio.com
*/

defined( 'ABSPATH' ) or die( 'Plugin file cannot be accessed directly.' );

if ( ! class_exists( 'PrerollYoutube' ) ) {
	class PrerollYoutube {
		//Tag identifier used by file includes and selector attributes.
		protected $tag = 'preroll-youtube';

		// User friendly name used to identify the plugin.
		protected $name = 'Preroll Youtube';

		// Current version of the plugin.
		protected $version = '1.0';

		// List of options to determine plugin behaviour.
		protected $options = array();

		// List of options to be used by Javascript.
		protected $local_options = array();

		// List of settings displayed on the admin settings page.
		protected $settings = array(
			'preroll' => array(
				'title' => 'Default Preroll Video ID',
				'description' => 'Default video.  Ensure this is a proper Youtube Video ID.  Leave empty if none.  Can be overwritten by the shortcode.',
				'placeholder' => ''
			),
			'controls' => array(
				'title' => 'Show Controls',
				'description' => 'Default controls behaviour.  0=hide, 1=show.  Can be overwritten by the shortcode.',
				'validator' => 'numeric',
				'placeholder' => "1"
			),
			'showinfo' => array(
				'title' => 'Show Video Info',
				'description' => 'Display the video title information. 0=hide, 1=show. Can be overwritten by the shortcode.',
				'validator' => 'numeric',
				'placeholder' => "1"
			),
			'theme' => array(
				'title' => 'Dark/Light Player',
				'description' => 'Display player controls (like a play button or volume control) within a dark or light control bar.  "dark"=normal/dark theme and "light"=light theme. Can be overwritten by the shortcode.',
				'placeholder' => "dark"
			),
		);

		// Initiate the plugin by setting the default values and assigning any required actions and filters.
		public function __construct() {
			if ( $options = get_option( $this->tag ) ) {
				$this->options = $options;
			}
			add_shortcode( $this->tag, array( &$this, 'shortcode' ) );
			
			if ( is_admin() ) {
				add_action( 'admin_init', array( &$this, 'settings' ) );
			}
		}

		// Allow the teletype shortcode to be used.
		public function shortcode( $atts, $content = null ) {
			extract( shortcode_atts( 
				array( 'video' => false, 'preroll' => false, 'height' => '', 'width' => '', 'class' => false, 'style' => false, 'link' => '', 'controls' => 1, 'showinfo' => 1, 'theme' => 'dark' ), $atts ) 
			);

	 		// Enqueue the required styles and scripts...
			$this->_enqueue();

	 		// Add custom styles...
			$styles = array();
			if ( is_numeric( $height ) && ($height > 0) ) {
				$styles[] = esc_attr( 'height: ' . $height . 'px;' );
			}
			if ( is_numeric( $width ) && ($width > 0) ) {
				$styles[] = esc_attr( 'width: ' . $width . 'px;' );
			}
			if ( !empty( $style ) ) {
				$styles[] = esc_attr( $style );
			}

	 		// Build the list of class names...
			$classes = array(
				$this->tag
			);
			if ( !empty( $class ) ) {
				$classes[] = esc_attr( $class );
			}

			// Set preroll by settings if not present in shortcode
	 		if (!isset($preroll) || strlen($preroll) == 0) {
	 			$preroll = $this->options['preroll'];
	 		}
			
	 		// Clean preroll variable
	 		if (strpos($preroll, 'youtube') !== false) {
	 			$parts = parse_url($preroll);
				parse_str($parts['query'], $query);
	 			$preroll = $query['v'];
	 		}
	 		if (strpos($preroll, 'youtu.be') !== false) {
	 			$preroll = parse_url($preroll, PHP_URL_PATH);
	 			$preroll = substr($preroll, 1);
	 		}

	 		// Clean video variable
	 		if (strpos($video, 'youtube') !== false) {
	 			$parts = parse_url($video);
				parse_str($parts['query'], $query);
	 			$video = $query['v'];
	 		}
	 		if (strpos($video, 'youtu.be') !== false) {
	 			$video = parse_url($video, PHP_URL_PATH);
	 			$video = substr($video, 1);
	 		}

			// Set theme by settings if not present in shortcode
	 		if (!isset($theme) || strlen($theme) == 0) {
	 			$theme = $this->options['theme'];
	 		}

			$local_options['preroll'] = $preroll;
			$local_options['mainroll'] = $video;
			$local_options['width'] = $width;
			$local_options['height'] = $height;
			$local_options['controls'] = $controls;
			$local_options['showinfo'] = $showinfo;
			$local_options['theme'] = $theme;
			$local_options['link'] = $link;
			
	 		// Output the terminal...	        
			ob_start();
			?>
			<style>
				.preroll-youtube-wrapper {margin: 0 0 1.5em 0;position: relative;padding-bottom: 56.25%;height: 0;overflow: hidden;}
				.preroll-youtube-wrapper iframe{border: none !important;position: absolute;top: 0;left: 0;width: 100%;height: 100%;max-width: 100%}
				.preroll-overlay div {background:rgba(0,0,0,0.5);color:#ffffff;z-index: 2000;position: absolute;right:10px;border: 1px solid white;padding: 5px 10px;}
				<?php if ($controls == "1") { ?>
					.preroll-overlay div{opacity: 0.5;bottom:50px;transition:.7s;}
					.preroll-youtube-wrapper:hover #preroll-overlay div{opacity:1.0;transition:.7s;}
				<?php } else { ?>
					.preroll-overlay div{opacity: 0.5;bottom:10px;transition:.7s;}
					.preroll-youtube-wrapper:hover #preroll-overlay div{opacity:1.0;transition:.7s;}
				<?php } ?>
			</style> 
			<div class="preroll-youtube-wrapper">
				<div class="preroll-overlay"></div>
				<script>var <?php echo preg_replace("/[-$]*/", "", $video);?> = <?php echo json_encode($local_options);?>;</script>
				<div data-id="<?php echo $video;?>" class="<?php esc_attr_e( implode( ' ', $classes ) );?>"<?php echo ( count( $styles ) > 0 ? ' style="' . implode( ' ', $styles ) . '"' : '' );?>></div>
			</div>
			<?php
			return ob_get_clean();
		}

		// Add the setting fields to the settings page.
		public function settings() {
			// use the media section for the settings
			$section = 'media';
			add_settings_section(
				$this->tag . '_settings_section',
				$this->name . ' Settings',
				function () {
					//echo '<p>Configuration options for the ' . esc_html( $this->name ) . ' plugin.</p>';
					echo '<p><strong>Shortcode works as follows:</strong><br>';
					echo '[preroll-youtube preroll="<i>Pre-roll Video ID</i>" video="<i>Main Video ID</i>"]<br>';
					echo 'Available options are <strong>controls="0/1" showinfo="0/1" theme="dark/light" link="http://www.clientlink.here"</strong>. 0="off" and "1"=on.</p>';
				},
				$section
			);
			
			foreach ( $this->settings AS $id => $options ) {
				$options['id'] = $id;
				$title = $options['title'];
				add_settings_field(
					$this->tag . '_' . $id . '_settings',
					$title,
					array( &$this, 'settings_field' ),
					$section,
					$this->tag . '_settings_section',
					$options
				);
			}
			
			register_setting( $section, $this->tag, array( &$this, 'settings_validate' ) );
		}

		// Append a settings field to the the fields section.
		public function settings_field( array $options = array() ) {
			$atts = array(
				'id' => $this->tag . '_' . $options['id'],
				'name' => $this->tag . '[' . $options['id'] . ']',
				'type' => ( isset( $options['type'] ) ? $options['type'] : 'text' ),
				//'class' => 'small-text',
				'value' => ( array_key_exists( 'default', $options ) ? $options['default'] : null )
			);
			
			if ( isset( $this->options[$options['id']] ) ) {
				$atts['value'] = $this->options[$options['id']];
			}
			
			if ( isset( $options['placeholder'] ) ) {
				$atts['placeholder'] = $options['placeholder'];
			}
			
			if ( isset( $options['type'] ) && $options['type'] == 'checkbox' ) {
				if ( $atts['value'] ) {
					$atts['checked'] = 'checked';
				}
				$atts['value'] = true;
			}
			
			array_walk( $atts, function( &$item, $key ) {
				$item = esc_attr( $key ) . '="' . esc_attr( $item ) . '"';
			} );
			?>
			<label>
				<input <?php echo implode( ' ', $atts ); ?> style="width:110px;"/>
				<?php if ( array_key_exists( 'description', $options ) ) : ?>
				<?php esc_html_e( $options['description'] ); ?>
				<?php endif; ?>
			</label>
			<?php
		}

		// Validate the settings saved.
		public function settings_validate( $input ) {
			$errors = array();
			foreach ( $input AS $key => $value ) {
				if ( $value == '' ) {
					unset( $input[$key] );
					continue;
				}
				$validator = false;
				if ( isset( $this->settings[$key]['validator'] ) ) {
					$validator = $this->settings[$key]['validator'];
				}
				switch ( $validator ) {
					case 'numeric':
						if ( is_numeric( $value ) ) {
							$input[$key] = intval( $value );
						} else {
							$errors[] = $key . ' must be a numeric value.';
							unset( $input[$key] );
						}
					break;
					default:
						 $input[$key] = strip_tags( $value );
					break;
				}
			}
			if ( count( $errors ) > 0 ) {
				add_settings_error(
					$this->tag,
					$this->tag,
					implode( '<br />', $errors ),
					'error'
				);
			}
			return $input;
		}

		// Enqueue the required scripts and styles, only if they have not previously been queued.
		protected function _enqueue() {
		
	 		// Define the URL path to the plugin...
			$plugin_path = plugin_dir_url( __FILE__ );

			wp_register_script( 'youtube-iframe-api', 'https://www.youtube.com/iframe_api' );
			wp_register_script( 'preroll-youtube', $plugin_path.'preroll-youtube.js', array( 'youtube-iframe-api' ));

	 		// Enqueue the scripts if not already...
			wp_enqueue_script( 'preroll-youtube' );  
		}

	}
	new PrerollYoutube;
}
?>