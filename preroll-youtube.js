function onYouTubeIframeAPIReady() {
		var players = jQuery('.preroll-youtube');
		players.each(function(i, iframe){
			var vID = jQuery(iframe).data('id');
			var pyo = window[vID.replace(/-+/g, "")];
			var preroll_youtube_seenPreroll = false;
			var preroll_youtube_playingId = '';
			var preroll_youtube_player;
			var preroll_youtube_html = '<div>Video Sponsor</div>';
			if (pyo["link"] != "") {
				preroll_youtube_html = '<div><a href="' + pyo["link"] + '" style="color:#FFFFFF !important" target="_blank">Visit Video Sponsor</a></div>';
			}
			new YT.Player( iframe, 
			{
				height: '390',
				width: '640',
				playerVars: {
					'wmode': 'opaque',
					'autohide': 2, // Autohides the progress bar and controls.
					'controls': pyo["controls"], // Removes the controls entirely.  Set to 1 to enable them.
					'modestbranding': 1, // Removes Youtube logo while playing.
					'rel': 0, // Removes related videos at the end.
					'showinfo': pyo["showinfo"], // Removes title information at top.
					'theme': pyo["theme"], // "light" for white player, "dark" for regular.  Not visible while no controls.
				},
				videoId: vID,
				events: {
					'onStateChange': function(event) {
						if (event.data == 1 && preroll_youtube_seenPreroll == false) {
							event.target.pauseVideo();
							event.target.loadVideoById(pyo["preroll"]);
							preroll_youtube_playingId = pyo["preroll"];
							preroll_youtube_seenPreroll = true;
							event.target.playVideo();
							jQuery(event.target).siblings(".preroll-overlay").html(preroll_youtube_html);
						} else if (event.data==0&&preroll_youtube_playingId==pyo.preroll) {
							event.target.loadVideoById(pyo["mainroll"]);
							preroll_youtube_playingId = pyo["mainroll"];
							event.target.playVideo();
							jQuery(event.target).siblings(".preroll-overlay").html("");
						}
					}}
				})
		})
	
};

